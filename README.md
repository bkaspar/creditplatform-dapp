# My master thesis (d)app
## Development

### Blockchain
 - start Ganache
 - connect MetaMask to same port
 - `truffle compile` to compile new/changed contracts
 - `truffle migrate` to install them in the Blockchain
 
### Backend
- Smart contract written in Solidity (https://github.com/ethereum/solidity)

### Frontend
- Written in Svelte (http://svelte.dev)
- `npm install` to install dependencies
- `npm run dev` to start development server on http://localhost:8080. Changes are detected automatically and the browser reloaded
