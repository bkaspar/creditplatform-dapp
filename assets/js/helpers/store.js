import { Store } from 'svelte/store.js'

if (module.hot) {
    module.hot.accept()
}

const LOCALSTORAGEKEY = 'etherloan'

class EtherLoanStore extends Store {
    
}

function useLocalStorage(store, key) {
    const json = localStorage.getItem(key)
    if (json) {
        store.set(JSON.parse(json))
    }

    store.onchange(state => {
        localStorage.setItem(key, JSON.stringify(state))
    })
}

let store = new EtherLoanStore({
    name: 'EtherLoan',
    address: 0,
    offers: 0
})

useLocalStorage(store, LOCALSTORAGEKEY)

export default store