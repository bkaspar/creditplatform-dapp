import Web3 from 'web3'

let web3js = undefined

if (typeof web3 !== 'undefined') {
    if (web3.currentProvider) {
        web3js = new Web3(web3.currentProvider);
    }
}

export default web3js