import store from './helpers/store.js';
import App from '../components/App.html';

var app = new App({
    target: document.getElementById('app'),
    store
});

export default app;