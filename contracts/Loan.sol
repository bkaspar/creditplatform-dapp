pragma solidity ^0.4.18;

contract Loan {

    /* Event to notify when a new Request is made */
    event NewLoan(address addr, uint requestId, uint256 amount, uint8 rates);

    /* Event to notify when a Request receives a new Offer */
    event NewOffer(address addr, uint offerId, uint256 interest);

    /* Represents an Offer which is part of a Request */
    struct Offer {
        address addr; // sender address
        uint256 amount; // we save the sent ETH amount for redundancy
        uint256 interest;
        uint256 timestamp;
        uint256 requestId;
        bool accepted;
        bool declined;
    }

    /* Represents a Request for a loan */
    struct Request {
        address addr; // requestee address
        uint256 amount;
        uint256 timestamp;
        uint8 rates;
        uint8 creditScore;
        bool fulfilled;
    }
    
    /* Holds all Offers in an array */
    Offer[] public offers;

    /* Holds all Requests in an array */
    Request[] public requests;

    /* Maps Request IDs to Ethereum addresses */
    mapping (uint => address) requestToOwner;

    /* Maps Offer IDs to Ethereum addresses */
    mapping (uint => address) offerToOwner;

    /* Maps an incrementing integer to Ethereum addresses for counting requests */
    mapping (address => uint) ownerRequestCount;

    /* Maps an incrementing integer to Ethereum addresses for counting offers */
    mapping (address => uint) ownerOfferCount;
    
    /**
    * Adds a new loan Request with the specified amount and monthly rates
    */
    function addRequest(uint256 amount, uint8 rates) external {
        uint requestId = requests.length++;
        Request storage request = requests[requestId];
        request.addr = msg.sender;
        request.amount = amount;
        request.timestamp = block.timestamp;
        request.rates = rates;
        request.creditScore = 1;
        request.fulfilled = false;

        requestToOwner[requestId] = msg.sender;
        ownerRequestCount[msg.sender]++;

        NewLoan(msg.sender, requestId, amount, rates);
    }

    /**
    * Adds a new Offer connected to a Request with the specified interest rate
    * Is defined as "payable" so it can receive ETH. 
    */
    function addOffer(uint requestId, uint64 interest) payable external {
        Request storage request = requests[requestId];
        require(request.amount == msg.value);
        require(request.fulfilled == false);

        uint offerId = offers.length++;
        Offer storage offer = offers[offerId];
        offer.addr = msg.sender;
        offer.amount = msg.value;
        offer.interest = interest;
        offer.timestamp = block.timestamp;
        offer.requestId = requestId;
        offer.accepted = false;
        offer.declined = false;

        offerToOwner[offerId] = msg.sender;
        ownerOfferCount[msg.sender]++;

        NewOffer(msg.sender, offerId, interest);
    }

    /**
    * Accepts an Offer. Can only be accepted by Request's owner
    * Accepting means transfer of the requested ETH to the requestee
    */
    function acceptOffer(uint offerId) external returns(bool) {
        Offer storage offer = offers[offerId];
        Request storage request = requests[offer.requestId];

        require(requestToOwner[offer.requestId] == msg.sender);
        require(request.fulfilled == false);
        require(offer.accepted == false && offer.declined == false);

        // Transfer ETH to requestee
        // Set transferable amount to 0 before doing the transfer to avoid cheating
        uint amount = offer.amount;
        offer.amount = 0;
        request.addr.transfer(amount);

        offer.accepted = true;
        request.fulfilled = true;
        return true;
    }

    /**
    * Declines an Offer. Can only be declined by Request's owner
    * Declining means returning the ETH to the fulfiller
    */
    function declineOffer(uint offerId) external returns(bool) {
        Offer storage offer = offers[offerId];
        Request storage request = requests[offer.requestId];

        require(requestToOwner[offer.requestId] == msg.sender);
        require(request.fulfilled == false);
        require(offer.accepted == false && offer.declined == false);

        // Refund ETH back to original sender
        // Set transferable amount to 0 before doing the transfer to avoid cheating
        uint amount = offer.amount;
        offer.amount = 0;
        offer.addr.transfer(amount);

        offer.declined = true;
        return true;
    }

    /**
    * Enables the loan offerer to rescind their offer and get their ETH back
    * Can only be rescinded by the original sender
    */
    function rescindOffer(uint offerId) external returns(bool) {
        Offer storage offer = offers[offerId];
        Request storage request = requests[offer.requestId];

        require(offer.addr == msg.sender);
        require(offer.accepted == false && offer.declined == false);
        require(request.fulfilled == false);

        uint amount = offer.amount;
        offer.amount = 0;
        offer.addr.transfer(amount);

        offer.declined = true;
        return true;
    }

    /**
    * Returns all the Requests made by a specific Ethereum address
    */
    function getRequestsByOwner(address addr) external view returns(uint[]) {
        uint[] memory result = new uint[](ownerRequestCount[addr]);
        uint counter = 0;
        for (uint i = 0; i < requests.length; i++) {
            if (requestToOwner[i] == addr) {
                result[counter] = i;
                counter++;
            }
        }
        return result;
    }

    /**
    * Returns all the Offers that have been made by a given address
    */
    function getOffersByOwner(address addr) external view returns(uint[]) {
        uint[] memory result = new uint[](ownerOfferCount[addr]);
        uint counter = 0;
        for (uint i = 0; i < offers.length; i++) {
            if (offers[i].addr == addr) {
                result[counter] = i;
                counter++;
            }
        }
        return result;
    }

    /**
    * Returns all the Offers that have been sent to a given address
    */
    function getOffersForOwner(address addr) external view returns(uint[]) {
        uint[] memory result = new uint[](getOffersCountForOwner(addr));
        uint counter = 0;
        for (uint i = 0; i < offers.length; i++) {
            if (requestToOwner[offers[i].requestId] == addr) {
                result[counter] = i;
                counter++;
            }
        }
        return result;
    }

    /**
    * Returns the total amount of Offers for a given address
    */
    function getOffersCountForOwner(address addr) public view returns(uint) {
        uint counter = 0;
        for (uint i = 0; i < offers.length; i++) {
            if (requestToOwner[offers[i].requestId] == addr) {
                counter++;
            }
        }
        return counter;
    }

    /**
    * Returns the amount of 'new' Offers for a given address
    * New means neither accepted nor declined
    */
    function getNewOffersCountForOwner(address addr) external view returns(uint) {
        uint counter = 0;
        for (uint i = 0; i < offers.length; i++) {
            if (requestToOwner[offers[i].requestId] == addr) {
                if (offers[i].accepted == false && offers[i].declined == false) {
                    counter++;
                }
            }
        }
        return counter;
    }

    /**
    * Returns the total amount of Offers of a single Request
    */
    function getRequestOfferCount(uint requestId) external view returns(uint) {
        uint count = 0;
        for (uint i = 0; i < offers.length; i++) {
            if (offers[i].requestId == requestId) {
                count++;
            }
        }
        return count;
    }

    /**
    * Returns the total amount of Offers
    */
    function getOfferCount() external view returns(uint) {
        return offers.length;
    }

    /*
    * Returns the total amount of Requests
    */
    function getRequestCount() external view returns(uint) {
        return requests.length;
    }

    /**
    * Returns the data of an Offer struct by its ID (index in the array)
    */
    function getOffer(uint offerId) external view returns(address, uint256, uint256, uint256, bool, bool) {
        Offer memory offer = offers[offerId];
        return (
            offer.addr,
            offer.interest,
            offer.timestamp,
            offer.requestId,
            offer.accepted,
            offer.declined
        );
    }

    /**
    * Returns the data of a Request struct by its ID (index in the array)
    */
    function getRequest(uint requestId) external view returns(address, uint256, uint256, uint8, uint8, bool) {
        Request memory request = requests[requestId];
        return (
            request.addr, 
            request.amount,
            request.timestamp, 
            request.rates,
            request.creditScore,
            request.fulfilled
        );
    }

    /**
    * This functions disables sending Ether directly to this contract
    */
    function() public {
        revert();
    }

}
