const HDWalletProvider = require('truffle-hdwallet-provider')
const m = 'select you fold virus zero topple close blanket room elegant quick ring'
const url = 'https://ropsten.infura.io/6U3VzoDl3OyNvH7GdtRR'

module.exports = {
    solc: {
        optimizer: {
            enabled: true,
            runs: 200
        }
    },
    networks: {
        development: {
            host: '127.0.0.1',
            port: 7545,
            network_id: '*'
        },
        test: {
            provider: function() { 
                return new HDWalletProvider(m, url)
            },
            network_id: 3,
            gas: 4698712
        }
    }
};
