var path = require("path")
var webpack = require('webpack')
var BundleTracker = require('webpack-bundle-tracker')
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var sass = require('node-sass');

const prod = process.env.NODE_ENV === 'production';

module.exports = {
  context: __dirname,

  entry: {
    bundle: ['./assets/js/index.js']
  },

  resolve: {
    extensions: ['.js', '.html']
  },

  output: {
      path: path.resolve('./assets/bundles/'),
      filename: "app.js",
  },

  devServer: {
    contentBase: './assets/bundles'
  },
  devtool: prod ? false : 'inline-source-map',

  plugins: [
    // new ExtractTextPlugin('styles.css'),
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new BundleTracker({filename: './webpack-stats.json'}),
  ],

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'buble-loader',
          query: { objectAssign: 'Object.assign' }
        }
      },
      {
        test: /\.html$/,
        exclude: /node_modules/,
        use: {
          loader: 'svelte-loader',
          options: {
            store: true,
            dev: true,
            style: ({ content }) => {
              return new Promise((fulfil, reject) => {
                sass.render({
                  data: content,
                  includePaths: ['src'],
                  sourceMap: true,
                  outFile: 'x' // this is necessary, but is ignored
                }, (err, result) => {
                  if (err) return reject(err);

                  fulfil({
                    code: result.css.toString(),
                    map: result.map.toString()
                  });
                });
              });
            },
            // dev: true,
            // emitCss: true,
            // store: true
          },
        }
      },
    ]
  },
}